const gl = @import("zgl");

const LZERawModel = @This();

vao: gl.VertexArray,
vertex_count: usize,

pub fn init(vao: gl.VertexArray, vertex_count: usize) LZERawModel {
    return .{
        .vao = vao,
        .vertex_count = vertex_count,
    };
}
