const gl = @import("zgl");

const LZERawModel = @import("../models/LZERawModel.zig");

const LZERenderer = @This();

pub fn init() LZERenderer {
    gl.clearColor(0.325, 0.271, 0.51, 1.0);
    return LZERenderer{};
}

pub fn render(self: LZERenderer, model: LZERawModel) void {
    _ = self;
    gl.bindVertexArray(model.vao);
    gl.enableVertexArrayAttrib(model.vao, 0);
    gl.drawElements(gl.PrimitiveType.triangles, model.vertex_count, gl.ElementType.u32, 0);
    gl.disableVertexArrayAttrib(model.vao, 0);
    gl.bindVertexArray(gl.VertexArray.invalid);
}
