const std = @import("std");

const LZERawModel = @import("../models/LZERawModel.zig");

const gl = @import("zgl");

const LZELoader = @This();

pub fn loadToVAO(positions: []f32, indices: []i32) LZERawModel {
    var vao = createVAO();
    bindIndicesBuffer(indices);
    storeDataInAttribList(0, positions);
    unbindVAO();
    return .{
        .vao = vao,
        .vertex_count = indices.len,
    };
}

fn createVAO() gl.VertexArray {
    var vao = gl.genVertexArray();
    gl.bindVertexArray(vao);
    return vao;
}

fn storeDataInAttribList(attrib_number: u32, data: []f32) void {
    var vbo = gl.genBuffer();
    gl.bindBuffer(vbo, gl.BufferTarget.array_buffer);
    gl.bufferData(gl.BufferTarget.array_buffer, f32, data, gl.BufferUsage.static_draw);
    gl.vertexAttribPointer(attrib_number, 3, gl.Type.float, false, 0, 0);
    gl.bindBuffer(gl.Buffer.invalid, gl.BufferTarget.array_buffer);
}

fn bindIndicesBuffer(indices: []i32) void {
    var ebo = gl.genBuffer();
    gl.bindBuffer(ebo, gl.BufferTarget.element_array_buffer);
    gl.bufferData(gl.BufferTarget.element_array_buffer, i32, indices, gl.BufferUsage.static_draw);
}

fn unbindVAO() void {
    gl.bindVertexArray(gl.VertexArray.invalid);
}
