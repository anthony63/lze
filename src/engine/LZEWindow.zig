const std = @import("std");
const glfw = @import("glfw");
const gl = @import("zgl");

const LZELoader = @import("LZELoader.zig");
const LZERenderer = @import("LZERenderer.zig");
const LZERawModel = @import("../models/LZERawModel.zig");

const LZEWindow = @This();

window: glfw.Window,
renderer: LZERenderer,
raw_model: LZERawModel,
fps_cap: f64,

pub fn init(title: [*:0]const u8, width: u32, height: u32, fps_cap: f32) !LZEWindow {
    try glfw.init(.{});
    var window = glfw.Window.create(width, height, title, null, null, .{
        .context_version_major = 3,
        .context_version_minor = 3,
        .resizable = false,
        .opengl_profile = .opengl_core_profile,
    }) catch |err| {
        std.debug.panic("Failed to create window, code: {}", .{err});
        return;
    };

    try glfw.makeContextCurrent(window);

    var renderer = LZERenderer.init();

    var vertices = [_]f32{ -0.5, 0.5, 0, -0.5, -0.5, 0, 0.5, -0.5, 0, 0.5, 0.5, 0 };
    var indices = [_]i32{ 0, 1, 3, 3, 1, 2 };
    var raw_model = LZELoader.loadToVAO(&vertices, &indices);

    return LZEWindow{
        .window = window,
        .fps_cap = fps_cap,
        .renderer = renderer,
        .raw_model = raw_model,
    };
}

pub fn run(self: LZEWindow) !void {
    gl.clearColor(0.325, 0.271, 0.51, 1.0);
    while (!self.window.shouldClose()) {
        try glfw.pollEvents();
        try self.render();
    }
}

fn render(self: LZEWindow) !void {
    gl.clear(.{ .color = true, .depth = true });

    self.renderer.render(self.raw_model);

    try self.window.swapBuffers();
}

pub fn deinit(self: LZEWindow) void {
    self.window.destroy();
}
