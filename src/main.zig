const std = @import("std");
const LWEWindow = @import("engine/LZEWindow.zig");

const TITLE = "lze beta | 0.0.1";
const WIDTH = 800;
const HEIGHT = 600;
const FPS_CAP = 60;

pub fn main() !void {
    var window = try LWEWindow.init(TITLE, WIDTH, HEIGHT, FPS_CAP);
    try window.run();
}
